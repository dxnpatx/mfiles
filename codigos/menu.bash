#!/bin/bash
export drowkid='✧ | ᴅʀᴏᴡᴋɪᴅ | ✧'

[[ `uname -m` != 'armv7l' ]] && {
	declare -A sdir=( [0]="/etc/chukk-script" [control]="/etc/chukk-script/controlador" [bin]="/etc/chukk-script/bin" )
} || {
	declare -A sdir=( [0]="." [control]="controlador" [bin]="bin" )
}

	for(( x=1;x<254;x++ )); do
		color[$x]="\e[38;5;${x}m"
	done

	for(( x=1;x<8;x++ )); do
		color[a$x]="\e[1;3${x}m"
	done


declare -A sfile=( [usr]="${sdir[0]}/info.user" [p]="${sdir[0]}/protocolos.sh" [log]="${sdir[0]}/.log"  [banners]="${sdir[0]}/banner.bash" [usrcd]="${sdir[0]}/usercodes" )

cor=( "\033[0m" "\033[1;34m" "\033[1;32m" "\033[1;37m" "\033[1;36m" "\033[1;33m" "\033[1;35m" )
sxz=( "🚀" "👤" '⚡' '⚙️')
color[n]+='\e[0m'
sig="\e[1;30m[\e[1;31m!\e[1;30m]"
usrdir="data-usr"

mportas(){
unset portas
portas_var=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
	while read port; do
	var1=$(echo $port | awk '{print $1}') && var2=$(echo $port | awk '{print $9}' | awk -F ":" '{print $2}')
	[[ "$(echo -e $portas|grep "$var1 $var2")" ]] || portas+="$var1 $var2\n"
	done <<< "$portas_var"
i=1
echo -e "$portas"
}


	[[ ! -e ${sfile[log]} ]] && {
		for x in `echo "badvpn udps shadow shadowr squid dropbear openvpn stunnel shadowsocks sockspy v2ray cfa trojan pshiphon tcp xray webmin slowdns sslh socks udp-hysteria udp-custom udp-zipvpn sshl webs socks apacheport"`; do
			echo $x >> ${sfile[log]}
		done
	}

init(){
cor=( "\033[0m" "\033[1;34m" "\033[1;32m" "\033[1;37m" "\033[1;36m" "\033[1;33m" "\033[1;35m" )

function msg(){
local COLOR[0]='\033[1;37m' COLOR[1]='\e[93m' COLOR[2]='\e[32m' COLOR[3]='\e[31m' COLOR[4]='\e[34m' COLOR[5]='\e[95m' COLOR[6]='\033[1;97m' COLOR[7]='\033[36m' NEGRITO='\e[1m' SEMCOR='\e[0m'
	case $1 in
	#-bar) echo -e "${color[124]}==============================================" ;;
	   -r) echo -e "${color[1]}$2${color[n]}" ;;
	   -c) echo -e "${color[6]}$2${color[n]}";;
	   -v) echo -e "\e[1;32m$2\e[0m";;
	   -a) echo -e "${color[3]}$2${color[n]}";;
	   -m) echo -e "\e[1;30m[${color[134]}#$2\e[1;30m]\e[0m";;
	 --mm) echo -e "${color[105]}$2 ${color[219]}$3";;
          -st) echo -e "\033[38;5;239m\e[100m\e[97m  ${2}  \e[0m";;
	  -ne) echo -ne " \e[1;30m[\e[1;33m•\e[1;30m] \e[1;33m$2${color[2]} ";read $3 ;;
	  -az)cor="${COLOR[4]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	 -bra) echo -e "\e[1m\e[1;37m$2\e[0m";;
	 -bar) echo -e "${color[186]}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${color[n]}";;
	 -ama)cor="${COLOR[3]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	 -azu)cor="${COLOR[6]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	-azuc)cor="${COLOR[7]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	-verm)cor="${COLOR[3]}${NEGRITO}[!] ${COLOR[1]}" && echo -e "${cor}${2}${SEMCOR}";;
	-verd)cor="${COLOR[2]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
       -verm2)cor="${COLOR[3]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	esac
}

function fun_banners(){
	source ${sdir[0]}/banner.bash "$1"
}

function number_var(){
	if (echo "$1" | egrep '[^0-9]' &> /dev/null); then
		msg -f "error"
	else
		var_number="$1"
	fi
}

function selection_fun(){
local selection="null"
local range
for((i=0; i<=$1; i++)); do range[$i]="$i "; done
while [[ ! $(echo ${range[*]}|grep -w "$selection") ]]; do
	echo -ne "\033[1;30m ╰► Seleccione su opción: " >&2
	read selection
	tput cuu1 >&2 && tput dl1 >&2
done
echo $selection
}

function menu_func(){
  local options="${#*}"
  local array
  for((num=1; num<=$options; num++)); do
    echo -ne "\033[0;35m [\033[0;36m$num\033[0;35m]\033[0;33m "
    array=(${!num})
    case ${array[0]} in
	"-v") echo -e " \e[1;32m${array[@]:1} ";;
	"-m") echo -e " \e[1;97m$(echo ${array[@]}|awk -F "|" '{print $2}') \e[1;30m[${color[134]}#$(echo ${array[@]}|awk -F "|" '{print $3}')\e[1;30m]\e[0m";;
       "-vm") echo -e " \e[1;30m[\e[1;31m!\e[1;30m] \e[1;93m${array[@]:1} \e[1;30m[\e[1;31m!\e[1;30m]";;
      "-bar") echo -e " \e[1;97m${array[@]:1}\n$(msg ${array[0]})";;
           *) echo -e " \e[1;97m${array[@]}";;
    esac
  done
}

function dell(){
	for(( n=1;n<$1;n++ )); do tput cuu1 && tput dl1 ; done
}

function continue(){
	msg -bar
	echo -ne "${color[134]}"
	read -p $' ========>> enter para continuar <<========'
}


function enter(){
	msg -bar
	printf "%13s \033[0;35m [\033[0;36m0\033[0;35m]\033[0;33m ${flech} $(msg -bra "\033[1;41m[ REGRESAR ]\e[0m")\n"
	msg -bar
}

function print_center(){
if [[ -z $2 ]]; then
    text="$1"
  else
    col="$1"
    text="$2"
  fi

  while read line; do
    unset space
    x=$(( ( 54 - ${#line}) / 2))
    for (( i = 0; i < $x; i++ )); do
      space+=' '
    done
    space+="$line"
    if [[ -z $2 ]]; then
      echo -e "${color[3]}$space"
    else
      echo -e "${color[$col]}$space"
    fi
  done <<< $(echo -e "$text")
}

function fun_bar(){
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
${comando[0]} -y > /dev/null 2>&1
${comando[1]} -y > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
echo -ne "\033[1;33m ["
while true; do
   for((i=0; i<18; i++)); do
   echo -ne "\033[1;31m##"
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "\033[1;33m ["
done
echo -e "\033[1;33m]\033[1;31m -\033[1;32m 100%\033[1;37m"
}

[[ $1 == '--puertos' ]] && {
unset puertos texto texto_ svcs porta
local texto
local texto_
local puertos
local svcs
local PT=$(lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN")
local _ps="$(ps x)"
x=1
for porta in `echo -e "$PT" | cut -d: -f2 | cut -d' ' -f1 |sort -n | uniq`; do
        [[ -z $porta ]] && continue
        porta[$x]="$porta"
        #echo "$porta - $(echo -e "$PT" | grep -w "$porta" | awk '{print $1}' | uniq | tail -1)"
        svcs[$x]="$(echo -e "$PT" | grep -w "$porta" | awk '{print $1}' | uniq | tail -1)"
        let x++;
done

for((i=1; i<$x; i++)); do
[[ ! -z ${svcs[$i]} ]] && texto="\033[1;34m ${pPIniT} \033[1;37m${svcs[$i]}: \033[1;31m${porta[$i]}" || texto=''
[[ ${svcs[$i]} = "apache2" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mAPACHE: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "node" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mWebSocket: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "clash" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mClash: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "psiphond" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mPSIPHON: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray-v2-u" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mXRAY/UI: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "v2-ui" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mV2-UI/WEB: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray-linu" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mXRAY/UI: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "x-ui" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mXUI/WEB: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "openvpn" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mOPENVPN-TCP: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "squid" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSQUID: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "squid3" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSQUID: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "dropbear" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mDROPBEAR: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "python3" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSOCKS/PYTHON3: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "python" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSOCKS/PYTHON: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "obfs-serv" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSR (OBFS): \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "ss-server" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSR (LIV): \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "sshd" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSH: ${porta[$i]}"
[[ ${svcs[$i]} = "ssh" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSH: ${porta[$i]}"
[[ ${svcs[$i]} = "systemd-r" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSystem-DNS: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "stunnel4" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSL: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "stunnel" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSL: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "v2ray" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mV2RAY: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mXRAY: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "badvpn-ud" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mBadVPN: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "trojan" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mTrojan-GO: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "sslh" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSSLH: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "nc.tradit" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mKeyGen: \033[1;31mON"
[[ ${svcs[$i]} = "filebrows" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mFileBrowser: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "rpcbind" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mRPCBind: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "snell-ser" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSNell: \033[1;31m${porta[$i]}"
    i=$(($i+1))
[[ ! -z ${svcs[$i]} ]] && texto_="\033[1;34m ${pPIniT} \033[1;37m${svcs[$i]}: \033[1;31m${porta[$i]}" || texto_=''
[[ ${svcs[$i]} = "apache2" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mAPACHE: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "node" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mWebSocket: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "clash" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mClash: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "psiphond" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mPSIPHON: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray-v2-u" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mXRAY/UI: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "v2-ui" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mV2-UI/WEB: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray-linu" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mXRAY/UI: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "x-ui" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mXUI/WEB: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "openvpn" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mOPENVPN-TCP: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "squid" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSQUID: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "squid3" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSQUID: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "dropbear" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mDROPBEAR: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "python3" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSOCKS/PYTHON3: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "python" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSOCKS/PYTHON: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "obfs-serv" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSR (OBFS): \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "ss-server" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSR (LIV): \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "sshd" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSH: ${porta[$i]}"
[[ ${svcs[$i]} = "ssh" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSH: ${porta[$i]}"
[[ ${svcs[$i]} = "systemd-r" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSystem-DNS: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "stunnel4" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSL: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "stunnel" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSL: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "v2ray" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mV2RAY: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "xray" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mXRAY: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "badvpn-ud" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mBadVPN: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "trojan" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mTrojan-GO: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "sslh" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSSLH: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "nc.tradit" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mKeyGen: \033[1;31mON"
[[ ${svcs[$i]} = "filebrows" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mFileBrowser: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "rpcbind" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mRPCBind: \033[1;31m${porta[$i]}"
[[ ${svcs[$i]} = "snell-ser" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSNell: \033[1;31m${porta[$i]}"
#[[ -z $texto_ ]] && {
#[[ -z $(echo -e "${_ps}"| grep slowdns | grep -v grep) ]] || texto_="\033[1;34m ∘ \033[1;37mSlowDNS: \033[1;33m5300"
#                                       }
#[[ -z $(echo -e "${_ps}"| grep slowdns | grep -v grep) ]] || texto="\033[1;34m ∘ \033[1;37mSlowDNS: \033[1;33m5300"
puertos+="${texto}|${texto_}\n"
done
local _PT=$(lsof -V -i UDP -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND"|grep -E 'openvpn|dns-serve|udpServer|hysteria|UDP-Custo|Hysteria2')
x=1
for porta in `echo -e "$_PT" | cut -d: -f2 | cut -d' ' -f1 |sort -n | uniq`; do
        [[ -z $porta ]] && continue
        _porta[$x]="$porta"
        _svcs[$x]="$(echo -e "$_PT" | grep -w "$porta" | awk '{print $1}' | uniq | tail -1)"
        let x++;
done
for((i=1; i<$x; i++)); do
[[ ! -z ${_svcs[$i]} ]] && texto="\033[1;34m ${pPIniT} \033[1;37m${_svcs[$i]}: \033[1;31m${_porta[$i]}" || texto=''
[[ ${_svcs[$i]} = "dns-serve" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mSlowDNS: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "openvpn" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mOPENVPN-UDP: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "udpServer" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mUDPServer: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "hysteria" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mHysteriaUDP: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "UDP-Custo" ]] && texto="\033[1;34m ${pPIniT} \033[1;37mUDP-Custom: \033[1;31m${_porta[$i]}"
i=$(($i+1))
[[ ! -z ${_svcs[$i]} ]] && texto_="\033[1;34m ${pPIniT} \033[1;37m${_svcs[$i]}: \033[1;31m${_porta[$i]}" || texto_=''
[[ ${_svcs[$i]} = "dns-serve" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mSlowDNS: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "openvpn" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mOPENVPN-UDP: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "udpServer" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mUDPServer: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "hysteria" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mHysteriaUDP: \033[1;31m${_porta[$i]}"
[[ ${_svcs[$i]} = "UDP-Custo" ]] && texto_="\033[1;34m ${pPIniT} \033[1;37mUDP-Custom: \033[1;31m${_porta[$i]}"
puertos+="${texto}|${texto_}\n"
done

	[[ $(echo -e "$puertos" | grep 'SSH: 22') ]] && {
		export PATH=$PATH:/usr/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/sbin:/bin:/usr/games
			[[ -z $(locale | grep "LANG=" | cut -d "=" -f2) ]] && export LANG=en_US.UTF-8
			echo -e "$puertos"|column -s "|"  -t
			} || echo -e "     ⚠️ PUERTOS SSH TRUNCADO POR DROPBEAR ⚠️   \n           Coloca : sudo -i \n         Ve al menu 7, opcion 7 "
			local PIDGEN=$(echo -e "${_ps}"| grep "BotGen.sh" | grep -v grep |awk '{print $1}')
	[[ ! -z $PIDGEN ]] && {
			local botG="\033[1;34m ${pPIniT} \033[1;37m BotGen Telegram 🤖 : \033[1;31m ⚡ ACTIVO ⚡"
			msg -bar3
			echo -e "$botG"
			unset svcs porta puertos i x
	}
}

}

init

[[ ! -e "${sfile[usr]}" ]] && { ress=${ress:=$drowkid}
	clear;toilet -f ansi "WELCOME"|lolcat
	msg -ne "Ingresa un nombre para el servidor: " name
	printf "${ress}\n$(wget -qO- ifconfig.me)\n${name}\n$(date +"%d-%m-%y")\n" >> ${sfile[usr]}
}

	[[ -s "${sfile[usr]}" ]] && {
		exec 6<&0 < "${sfile[usr]}"
			read ress;read ip;read slogan
		exec 0<&6 6<&-
	}

function banner_script(){
clear
	if [[ -n $slogan ]]; then
		echo -ne "$(toilet -f future $slogan) " | lolcat;echo -ne "  \e[38;5;138m$ress
"
	else
		toilet -f future 'PatoScript'|lolcat
	fi
}

function home(){
clear
	if [[ -n $slogan ]]; then
		toilet -f future $slogan | lolcat
	else
		exit 1
	fi
	msg -bar
}

function checkport(){
[[ -z $1 ]] && return
		for instalcolor in `echo "badvpn shadow shadowr squid dropbear openvpn stunnel shadowsocks sockspy v2ray cfa trojan pshiphon udps tcp xray webmin slowdns sslh socks udp-hysteria udp-custom udp-zipvpn sshl webs socks"`; do
			[[ $(cat ${sfile[log]}|grep $instalcolor) != "" ]] && declare -A proto[$instalcolor]+="\e[1;31mNO INSTALADO" || declare -A proto[$instalcolor]+="\e[1;32mINSTALADO"
		done
echo -nE "\e[0m\e[38;5;10mESTADO: ${proto[$1]}"
}

function banner(){
source <(curl -sSL https://gist.githubusercontent.com/drowkid01/13427340e52030ae713212e37f9930f7/raw/8cb72b79e9501c0085a3afb29c10cc68fd9887d4/banner.bash) --$1
checkport "$1"
}

menu_info()
{ msg -bar
export system
if [[ "$(grep -c "Ubuntu" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f2 /etc/issue.net |awk -F "." '{print $1}')
elif [[ "$(grep -c "Debian" /etc/issue.net)" = "1" ]]; then
system=$(cut -d' ' -f1 /etc/issue.net)
system+=$(echo ' ')
system+=$(cut -d' ' -f3 /etc/issue.net)
else
system=$(cut -d' ' -f1 /etc/issue.net)
fi

#_ram=$(printf '%-10s' "$(free -h | grep -i mem | awk {'print $2'})")
_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")
_ram=$(printf ' %-8s' "$(free -h | grep -i mem | awk {'print $2'})")
_ram2=$(printf ' %-8s' "$(free -h | grep -i mem | awk {'print $4'})")
_system=$(printf '%-9s' "$system")
#_usop=$(printf '%-1s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")
#_core=$(printf '%-8s' "$(grep -c cpu[0-9] /proc/stat)")
_core=$(printf '%-8s' "$(grep -c cpu[0-9] /proc/stat)")
_usop=$(top -bn1 | sed -rn '3s/[^0-9]* ([0-9\.]+) .*/\1/p;4s/.*, ([0-9]+) .*/\1/p' | tr '\n' ' ')

modelo1=$(printf '%-11s' "$(lscpu | grep Arch | sed 's/\s\+/,/g' | cut -d , -f2)")
mb=$(printf '%-8s' "$(free -h | grep Mem | sed 's/\s\+/,/g' | cut -d , -f6)")
_hora=$(printf '%(%H:%M:%S)T')
_hoje=$(date +'%d/%m/%Y')
system=$(cat -n /etc/issue |grep 1 |cut -d ' ' -f6,7,8 |sed 's/1//' |sed 's/      //') 
vercion=$(echo $system|awk '{print $2}'|cut -d '.' -f1,2)
echo -e "\033[1;37m OS \033[1;31m: \033[1;32m$_system \033[1;37mHORA\033[1;31m: \033[1;32m$_hora  \033[1;37mIP\033[1;31m:\033[1;32m $ip"
echo -e "\033[1;37m RAM\e[31m: \033[1;32m$_ram \033[1;37mUSADO\033[1;31m: \033[1;32m$mb\033[1;37m LIBRE\033[1;31m: \033[1;32m$_ram2"

}

#===========>> MENÚ PRINCIPAL
start_menu()
{

	#=======>> CHECK-UPDATE SCRIPT.
	check_update(){
		fun_banners --instal
		print_center "${color[134]}VERIFICANDO LA EXISTENCIA DE NUEVAS ACTUALIZACIONES"
		fun_bar
		dell 3
		msg -v "[✓] CUENTAS CON LA ÚLTIMA ACTUALIZACIÓN [✓]"
		continue
		start_menu
	}
	#========>> CRÉDITOS.
	creditos(){
		fun_banners --instal
		print_center "${color[134]} CRÉDITOS"
		msg -bar
		echo -e "\e[1;32mpatoScript \e[1;97mes un script basado en las estructuras\ncompartidas por @Kalix1 en su GitHub.\n\e[1;32mTmpatoScript\e[1;97m también utiliza algunos archivos \ny/o recursos de rufu99.\n\nTodos los archivos en su mayoría fueron creados desde cero\nunica y exclusivamente por \e[1;32m@drowkid01\e[1;97m!. \n\nSoporte: https://t.me/drowkid01"
		continue
	}

	#=======>> CONFIGURACIONES DEL SERVIDOR.
	configuraciones(){
				speed_test(){ clear
						mkdir -p /opt/speed/ >/dev/null 2>&1
						wget -O /opt/speed/speedtest https://raw.githubusercontent.com/NetVPS/LATAM_Oficial/main/Ejecutables/speedtest.py &>/dev/null
						chmod +rwx /opt/speed/speedtest
						ping=$(ping -c1 google.com | awk '{print $8 $9}' | grep -v loss | cut -d = -f2 | sed ':a;N;s/\n//g;ta')
						starts_test=$(/opt/speed/speedtest)
						down_load=$(echo "$starts_test" | grep "Download" | awk '{print $2,$3}')
						up_load=$(echo "$starts_test" | grep "Upload" | awk '{print $2,$3}')

					echo -e "\e[1;93m    PRUEBA DE VELOCIDAD DE HOSTING  [by patoScript]"
					msg -bar
					fun_bar "$starts_test"
					msg -bar
					msg -ama " Latencia:\033[1;92m $ping\n Subida:\033[1;92m $up_load\n Descarga:\033[1;92m $down_load"
					continue
					[[ -z $1 ]] && configuraciones || protocolos
				}
		[[ $1 == "--speed-test" ]] && speed_test --init
		[[ $1 == '--optimizar-vps' ]] && {
			echo -e "OPTIMIZANDO SERVIDOR"  | pv -qL 80
	                tput civis
					(
			sync
			echo 3 >/proc/sys/vm/drop_caches
			sync && sysctl -w vm.drop_caches=3
 			sysctl -w vm.drop_caches=0
			swapoff -a
			swapon -a
		        [[ -e /etc/v2ray/config.json ]] && v2ray clean 1> /dev/null 2> /dev/null
		        [[ -e /etc/xray/config.json ]] && xray clean 1> /dev/null 2> /dev/null
		        rm -rf /tmp/* > /dev/null 2>&1
		        killall usercodes > /dev/null 2>&1
		        killall ferramentas > /dev/null 2>&1
		        killall menu_inst > /dev/null 2>&1
		        killall kswapd0 > /dev/null 2>&1
		        killall tcpdump > /dev/null 2>&1
		        killall ksoftirqd > /dev/null 2>&1
		        #echo -e "@drowkid01 " > /var/log/auth.log
		        systemctl restart rsyslog.service
		        systemctl restart systemd-journald.service
		        service dropbear stop > /dev/null 2>&1
		        service sshd restart > /dev/null 2>&1
		        service dropbear restart > /dev/null 2>&1
		        sleep 2
				) &>/dev/null 2>&1
		                while [ -d /proc/$! ]; do
                 		       for i in / - \\ \|; do
		                                sleep .1;echo -ne "\e[1D$i"
		                        done
		                done
			echo -ne "\033[1;37mOPTIMIZANDO MEMORIA \033[1;32mRAM \033[1;37me \033[1;32mSWAP\033[1;32m.\033[1;33m.\033[1;31m. \033[1;33m";sleep 1
	                tput cnorm
			echo -e "\e[1DOk";return 0

		}

		. ${sfile[banners]} --conf
		msg -st "MENÚ DE CONFIGURACIONES GENERALES"

				data_server(){
					totalram=$(free | grep Mem | awk '{print $2}') && usedram=$(free | grep Mem | awk '{print $3}') && freeram=$(free | grep Mem | awk '{print $4}') && swapram=$(cat /proc/meminfo | grep SwapTotal | awk '{print $2}');clock=$(lscpu | grep "CPU MHz" | awk '{print $3}') && based=$(cat /etc/*release | grep ID_LIKE | awk -F "=" '{print $2}') && processor=$(cat /proc/cpuinfo | grep "model name" | uniq | awk -F ":" '{print $2}') && cpus=$(cat /proc/cpuinfo | grep processor | wc -l)
					for ruta in `echo "/etc/issue.net /proc/cpuinfo /proc/meminfo"`; do
						if [[ ! ${ruta} ]]; then
							msg -f "Error al procesar la información.";msg -bar
						sleep 1;return 0
						fi
					 done
						[[ "$system" ]] && msg -ama "Su sistema es:	$system"
						[[ $based ]] && echo -e "${cor[5]} BASADO	${null}$based"
						[[ $processor ]] && msg -ama "Procesador físico:	$processor x$cpus"
						[[ $clock ]] && msg -ama "Frecuencia máxima:	$clock MHz"
					echo -e "Uso del CPU:		$(ps aux  | awk 'BEGIN { sum = 0 }  { sum += sprintf("%f",$3) }; END { printf " " "%.2f" "%%", sum}')"|lolcat
					echo -e "Arquitectura CPU ID:	$(lscpu | grep "Vendor ID" | awk '{print $3}')"|lolcat
					echo -e "Memoria ram total:	$(($totalram / 1024))"|lolcat
					echo -e "Memoria ram usada:	$(($usedram / 1024))"|lolcat
					echo -e "Memoria ram libre:	$(($freeram / 1024))"|lolcat
					echo -e "Memoria swap:		$(($swapram / 1024))MB"|lolcat
					echo -e "Tiempo online:		$(uptime)"|lolcat
					echo -e "Nombre del servidor:	$(hostname)"|lolcat
					echo -e "Dirección privada:	$(ip addr | grep inet | grep -v inet6 | grep -v "host lo" | awk '{print $2}' | awk -F "/" '{print $1}' | head -1)"|lolcat
					echo -e "Dirección pública:	$(wget -qO- ifconfig.me)"|lolcat
					echo -e "Versión del kernel:	$(uname -r)"|lolcat
					echo -e "Arquitectura:		$(uname -m)"|lolcat
					msg -bar
					continue
				}
				hora_local(){
				banner_script
				msg -bar
					function change_local_time(){
						rm -rf /etc/localtime
						ln -sf /usr/share/zoneinfo/America/$1 /etc/localtime
						echo -e "\e[1;92m          >> FECHA LOCAL $1 APLICADA! <<\n\e[93m           $(date)\n$(msg -bar)\n"
						read -p $'\e[1;30m ==>> enter para continuar <<=='
					}
					menu_func "CAMBIAR HORA LOCAL MX" "CAMBIAR HORA LOCAL ARG" "CAMBIAR HORA LOCAL COL" "CAMBIAR HORA LOCAL PERÚ" "-bar CAMBIAR HORA LOCAL GT"
					h_user=`selection_fun 4`
						if [[ -z $h_user ]]; then
							msg -f "error", dell 1
						elif [[ $h_user == '1' ]]; then
							change_local_time "Merida"
						elif [[ $h_user == '2' ]]; then
							change_local_time "Argentina/Buenos_Aires"
						elif [[ $h_user == '3' ]]; then
							change_local_time "Bogota"
						elif [[ $h_user == '4' ]]; then
							change_local_time "Lima"
						fi
				}
				edit_hostname(){
				unset name
					while [[ ${name} = "" ]]; do
						msg -ne "Ingrese el nuevo nombre del servidor: " name
						tput cuu1 && tput dl1
					done
					hostnamectl set-hostname $name
					echo $name > name
					if [ $(hostnamectl status | head -1  | awk '{print $3}') = "${name}" ]; then echo -e "\033[1;33m Host alterado corretamente !, reiniciar VPS" ; else echo -e "\033[1;33m Host no modificado!" ;  fi
					return
				}
				edit_ssh(){
				banner_script
				unset portserv
				echo -e "\n$(print_center -ama "RECONFIGURACIÓN PUERTO SSH")\n"
					msg -ne "¿Desea reconfigurar su puerto ssh? [s/n]: " opx
					case $opx in
					 "s"|"S"|"Si"|"Yes"|"yes"|"simon") srut="/etc/ssh/sshd_config"
						msg -bar && msg -bra "$(echo -e "\e[1;33mIngresa el puerto que será utilizado para OpenSSH: ")"
						read -p $'\e[1;31mPredeterminado: ' -e -i '22' portserv
						sed -e "/Port /d;" -e "1i Port ${portserv}" -i ${srut}
						msg -bar
						print_center "RECONFIGURANDO PUERTO \e[1;32m22 \e[1;33m-> \e[1;32m${portserv}"
						print_center 1 "${sig} DETENIENDO DROPBEAR ${sig}" | pv -qL 40
						service dropbear stop > /dev/null 2>&1
						[[ -e /etc/init.d/dropbear ]] && /etc/init.d/dropbear stop > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
						msg -bar
						msg -v "${sig} REINICIANDO OPENSSH ${sig}" | pv -qL 40
						service sshd restart > /dev/null 2>&1;service ssh restart > /dev/null 2>&1
						[[ -e /etc/init.d/ssh ]] && /etc/init.d/ssh restart > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
						msg -bar
						msg -v "RESTAURANDO PUERTOS DROPBEAR" | pv -qL 40
						service dropbear restart > /dev/null 2>&1
						[[ -e /etc/init.d/dropbear ]] && /etc/init.d/dropbear restart > /dev/null 2>&1 && echo -e "\033[1;32m [OK]" || echo -e "\033[1;31m [FAIL]"
						msg -bar;exit 0;echo -e "\n"
						read -p $'\e[1;30m====>> enter para continuar <<===='
					;;
					*)msg -f "regresando al menú anterior..";sleep 2;configuraciones;;
					esac
				configuraciones
				}
		menu_info
		msg -bar
		menu_func "RECONFIGURACIÓN PUERTO OPENSSH $(msg -m 'fix')" "EDITAR HOSTNAME" "MODIFICAR HORA LOCAL" "MOSTRAR DETALLES DEL SERVIDOR" "-bar FIX UBUNTU" "PERSONALIZACIÓN BANNER DROPBEAR" "ACTIVACIÓN NOTIBOT" "SPEED-TEST"
		enter
		case `selection_fun 8` in
		1)edit_ssh;;
		0)start_menu;;
		2)edit_hostname;;
		3)hora_local;;
		4)data_server;;
		5)source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu/sourcesfix.sh) && continue ;;
		6)banner_dropbear;;
		7)notipato;;
		8)speed_test;;
		esac
	}

#home
banner_script
menu_info
msg -bar
init --puertos
msg -bar
menu_func "-m |CONTROLADOR DE USUARIOS|NEW MENÚ" "\e[1;30m[\e[1;31m!\e[1;30m] \e[1;93mOPTIMIZACIÓN DEL SERVIDOR" "MONITOR HTOP" "-bar INSTALADOR DE PROTOCOLOS" "CONFIGURACIONES GENERALES" "-bar \e[1;32mUPDATE\e[1;33m / \e[1;31mREMOVE \e[1;97mSCRIPT" "-bar \e[1;92mCRÉDITOS"

	#========>> MONITOR HTOP
	monitorhtop()
	{
		banner_script
		print_center -ama "\033[1;93mMONITOR DE PROCESOS HTOP\n"
		msg -bar
		msg -bra "		RECUERDA SALIR CON : \033[1;96m CTRL + C o FIN + F10"
		[[ $(dpkg --get-selections | grep -w "htop" | head -1) ]] || apt-get install htop -y &>/dev/null
		read -t 10 -n 1 -rsp $'\033[1;39m       << Presiona enter para Continuar >>\n'
		clear && clear
		sudo htop
		msg -bar
		echo -ne " \033[1;93m     MONITOR DE PROCESOS HTOP\n"
		msg -bar
		echo -e "\e[97m                  FIN DEL MONITOR"
		 msg -bar
		start_menu
	}

	#=========>> HERRAMIENTAS.
	herramientas()
	{
			blockbT(){
				fun_banners --blockbt
				msg -st 'PANEL FIREWALL 2023 [@drowkid01]'
				msg -bar
				menu_func "BLOQUEAR TORRENT $(msg -m 'PALABRAS CLAVE')" "BLOQUEAR PUERTOS $(msg -m 'SPAM')" "BLOQUEAR TORRENT $(msg -m 'PUERTOS SPAM + PALABRAS CLAVE')" "BLOQUEAR PUERTOS $(msg -m 'PERSONALIZADOS')" "-bar BLOQUEAR PALABRAS CLAVE $(msg -m 'PERSONALIZADAS')" "DESBLOQUEAR TORRENT $(msg -m 'PALABRAS CLAVE')" "DESBLOQUEAR PUERTOS $(msg -m 'SPAM')" "DESBLOQUEAR TORRENT + PALABRAS CLAVE + PUERTOS SPAM" "DESBLOQUEAR PUERTOS $(msg -m 'PERSONALIZADOS')" "DESBLOQUEAR PALABRAS CLAVE $(msg -m 'PERSONALIZADAS')" "DESBLOQUEAR TODAS LAS PALABRAS CLAVE $(msg -m 'PERSONALIZADAS')"
				enter
				case `selection_fun 14` in
				1) ;;
				esac
			}
	 [[ $1 == '--blockbT' ]] && blockbT
	}
	#======>> PROTOCOLOS [menú gráfico]
	protocolos()
	{
		autoconfig_service()
		{ clear
			menu_info &> /dev/null
			version=$vercion;so=$system
			print_center -ama "ESPERE UN MOMENTO MIENTRAS FIXEAMOS SU SISTEMA "
				fun_upgrade() {
					sync
					echo 3 >/proc/sys/vm/drop_caches
					sync && sysctl -w vm.drop_caches=3
					sysctl -w vm.drop_caches=0
					swapoff -a
					swapon -a
					sudo apt install software-properties-common -y &> /dev/null
					apt install python2 -y &> /dev/null
					sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 1 &> /dev/null
					rm -rf /tmp/* > /dev/null 2>&1
					killall kswapd0 > /dev/null 2>&1
					killall tcpdump > /dev/null 2>&1
					killall ksoftirqd > /dev/null 2>&1
					echo > /etc/fixpython
				}
			source <(curl -sSL ${url[config]}) --init &> /dev/null
				inst_ssl () { pkill -f stunnel4
					apt purge stunnel4 -y &> /dev/null;apt install stunnel4 -y > /dev/null 2>&1
					echo -e "cert = /etc/stunnel/stunnel.pem\nclient = no\nsocket = a:SO_REUSEADDR=1\nsocket = l:TCP_NODELAY=1\nsocket = r:TCP_NODELAY=1\n\n[stunnel]\naccept = 443\nconnect = 127.0.0.1:80\n" > /etc/stunnel/stunnel.conf
					openssl genrsa -out key.pem 2048 > /dev/null 2>&1
					(echo "$(curl -sSL ipinfo.io > info && cat info | grep country | awk '{print $2}' | sed -e 's/[^a-z0-9 -]//ig')" ; echo "" ; echo "$(wget -qO- ifconfig.me):81" ; echo "mx" ; echo "Speed" ; echo "" ; echo "" )|openssl req -new -x509 -key key.pem -out cert.pem -days 1095 > /dev/null 2>&1
					cat key.pem cert.pem >> /etc/stunnel/stunnel.pem
					sed -i 's/ENABLED=0/ENABLED=1/g' /etc/default/stunnel4
					service stunnel4 restart > /dev/null 2>&1
					rm -f info key.pem cert.pem
				}
				inst_py(){ sed -i '/PDirect80.py/d' /bin/autoboot
					fun_bar "wget -O $HOME/PDirect80.py 'https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/autoconfig-sh/PDirect.py'"
					screen -dmS "ws80" python $HOME/PDirect80.py & > /root/proxy.log 
				}
			checkk=$(checkport "python-ssl-direct")
			. ${sfile[banners]} --python-ssl
			menu_info
			msg -bar
			print_center "${sign}RECORDATORIO: ${sig}"
			echo -e "\e[1;97mEncontrarás el autoconfig de \e[1;32m@Kalix1\n\e[1;97m[usado en LACASITAMX] en la opción 3!."
			msg -bar
			menu_func "SSL + (PAYLOAD/DIRECTO) - AUTOINSTALL" "-bar CONFIGURAR PYTHON (RESPONSE STATUS 200)" "-bar SSL-WebSocket 80>443 [@kalix1]" "-vm \e[1;91mREMOVER AUTOCONFIGURACIÓN" "FIX AUTOCONFIG"
			enter
		 	case `selection_fun 5` in
			1) init --puertos
				print_center -ama "RECONFIGURANDO STUNNEL 443"
				fix_ssl;print_center -ama "RECONFIGURANDO PYTHON SOCKS 80"
				echo ''
				fix_py &> /dev/null
				print_center -verd "\e[1;32m • VERIFICANDO ACTIVIDAD DE SOCK PYTHON  •"
				autoboot &> /dev/null
				sleep 2;del 2;sleep 1
				[[ $(ps x | grep "ws80 python" |grep -v grep ) ]] && {
					msg -bar
					print_center -v "\e[1;32mREACTIVANDO SOCKS EN PUERTO 80"
				[[ $(grep -wc "ws80" /bin/autoboot) = '0' ]] && {
							echo -e "netstat -tlpn | grep -w 80 > /dev/null || {  screen -r -S 'ws80' -X quit;  screen -dmS ws80 python $HOME/PDirect80.py & >> /root/proxy.log ; }" >>/bin/autoboot
						} || {
							sed -i '/ws80/d' /bin/autoboot
							echo -e "netstat -tlpn | grep -w 80 > /dev/null || {  screen -r -S 'ws80' -X quit;  screen -dmS ws80 python $HOME/PDirect80.py & >> /root/proxy.log ; }" >>/bin/autoboot
					}
				crontab -l > /root/cron
				[[ -z $(cat < /root/cron | grep 'autoboot') ]] && echo "@reboot /bin/autoboot" >> /root/cron || {
				[[ $(grep -wc "autoboot" /root/cron) > "1" ]] && {
					sed -i '/autoboot/d' /root/cron
					echo "@reboot /bin/autoboot" >> /root/cron
					}
				}
					crontab /root/cron
					service cron restart
					sleep 2s && tput cuu1 && tput dl1
				} || {
					print_center -azu " FALTA ALGUN PARAMETRO PARA INICIAR REACTIVADOR "
					sleep 2s && tput cuu1 && tput dl1
					return
				}
				tput cuu1 && tput dl1
				msg -bar
				[[ $(ps x | grep -w  "PDirect80.py" | grep -v "grep" | awk -F "pts" '{print $1}') ]] && print_center -verd "PYTHON INICIADO CON EXITO!!!" || print_center -ama " ERROR AL INICIAR PYTHON!!!"
				msg -bar
				sleep 1
				print_center -v "[✓] INSTALACIÓN FINALIZADA [✓]"
				msg -bar
				echo -e "Solucionado el error de conectividad mediante el puerto $porta con SNI"
				break
			;;
			2)
				source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/autoconfig-sh/Proxy.sh)
			;;
			3)
				kill  $(ps x | grep -w "PDirect80" | grep -v grep | cut -d ' ' -f1) &>/dev/null
				sed -i '/PDirect80/d' /bin/autoboot
				screen -wipe &>/dev/null
				autoboot &>/dev/null
			;;
			0) protocolos;;
			esac

		}

	dell 22
	init --puertos

	for xp in `echo "squid dropbear openvpn stunel shadow telegran socks apacheport gettun tcpbypass webmin ddos v2ray sshl webs socks"`; do
		unset $xp
	done

	_portbaSE="$(netstat -tunlp)"
	_ps="$(ps x)"
	tojanss=`if echo -e "$_portbaSE" | grep trojan 1> /dev/null 2> /dev/null; then
	echo -e "\033[1;32m[ON] "
	else
	echo -e "\033[1;31m[OFF]"
	fi`;
	[[ $(echo -e "$_portbaSE" | grep trojan) ]] && pTROJ="\033[1;32m[ON] " || pTROJ="\033[1;31m[OFF]"
	pps=`if echo -e "$_portbaSE" | grep psiphond 1> /dev/null 2> /dev/null; then
	echo -e "\033[1;32m[ON] "
	else
	echo -e "\033[1;31m[OFF]"
	fi`;
	v2ray=`if echo -e "$_portbaSE" | grep v2ray 1> /dev/null 2> /dev/null; then
	echo -e "\033[1;32m[ON] "
	else
        if echo -e "$_portbaSE" | grep x-ui 1> /dev/null 2> /dev/null; then
        echo -e "\033[1;32m[\033[0;34mUI\033[1;32m] "
        else
        echo -e "\033[1;31m[OFF]"
        fi
	fi`;
	xclash=`if echo -e "$_portbaSE" | grep clash 1> /dev/null 2> /dev/null; then
	echo -e "\033[1;32m[ON] "
	else
	[[ -e /root/.config/clash/config.yaml ]] && echo -e "\033[1;32m[\033[0;34mCFA\033[1;32m]" || echo -e "\033[1;31m[OFF]"
	fi`;
	[[ $(echo -e "${_ps}"| grep udpServer| grep -v grep) ]] && _pidUDP="\033[0;34m[US] " || {
	        [[ $(echo -e "${_ps}"| grep UDP-Custom| grep -v grep) ]] && _pidUDP="\033[1;32m[\033[0;34mUC\033[1;32m] " || {
	                [[ $(echo -e "${_ps}"| grep hysteria| grep -v grep) ]] && _pidUDP="\033[1;32m[\033[0;34mHYS\033[1;32m] "
	                } || _pidUDP="\033[1;31m[OFF]"
	}
	slowssh=$(echo -e "${_ps}"| grep "slowdns-ssh"|grep -v grep > /dev/null && echo -e "\033[1;32m?? " || echo -e "\033[1;31m?? ")
	slowpid=$(echo -e "${_ps}" | grep -w "dns-server" | grep -v "grep" | awk -F "pts" '{print $1}') && [[ ! -z $slowpid ]] && P1="\033[0;32m[ON] " || P1="\033[1;31m[OFF]"
	[[ -e /etc/squid3/squid.conf ]] && squid="\033[0;32m[ON] " || squid="\033[1;31m[OFF]"
	[[ -e /etc/squid/squid.conf ]] && squid="\033[0;32m[ON] " || squid="\033[1;31m[OFF]"
	[[ $(echo -e "$_portbaSE" |grep dropbear|head -1) ]] && dropb="\033[1;32m[ON] " || dropb="\033[1;31m[OFF]"
	[[ -e /etc/openvpn/server.conf ]] && openvpn="\033[0;32m[ON] " || openvpn="\033[1;31m[OFF]"
	[[ $(echo -e "$_portbaSE" |grep stunnel|head -1) ]] && stunel="\033[1;32m[ON] " || stunel="\033[1;31m[OFF]"
	[[ -e /etc/shadowsocks.json ]] && shadow="\033[1;32m[ON]" || shadow="\033[1;31m[OFF]"
	[[ "$(echo -e "${_ps}" | grep "ultimatebot" | grep -v "grep")" != "" ]] && telegran="\033[1;32m[ON]"
	[[ $(echo -e "${_ps}" | grep "PDirect.py") ]] && socks="\033[1;32m[\033[0;34mPyD\033[1;32m]" || socks="\033[1;31m[OFF]"
	[[ $(echo -e "${_ps}" | grep "PDirect80") ]] && socksA="\033[1;32m[\033[0;34mRUN\033[1;32m]" || socksA="\033[1;31m[OFF]"
	[[ -e /${sdir[0]}/edbypass ]] && tcpbypass="\033[1;32m[ON]" || tcpbypass="\033[1;31m[OFF]"
	[[ -e /etc/webmin/miniserv.conf ]] && webminn="\033[1;32m[ON]" || webminn="\033[1;31m[OFF]"
	[[ -e /usr/local/x-ui/bin/config.json ]] && v2ui="\033[1;32m[ON]" || v2ui="\033[1;31m[OFF]"
	[[ -e /usr/local/etc/trojan/config.json ]] && troj="\033[1;32m[ON]" || troj="\033[1;31m[OFF]"
	[[ -e /etc/default/sslh ]] && sslh="\033[1;32m[ON] " || sslh="\033[1;31m[OFF]"
	[[ -e /usr/local/ddos/ddos.conf ]] && ddos="\033[1;32m[ON]"
	 ssssrr=`ps -ef |grep -v grep | grep server.py |awk '{print $2}'`
	[[ ! -z "${ssssrr}" ]] && cc="\033[1;32m" || cc="\033[1;31m"
	[[ -d /usr/local/shadowsocksr ]] && {
	user_info=$(cd /usr/local/shadowsocksr &> /dev/null  && python mujson_mgr.py -l )
	user_t="\033[1;33m$(echo "${user_info}"|wc -l) Cts"
	} || user_t="\033[1;31m[OFF]"
	[[ `grep -c "^#ADM" /etc/sysctl.conf` -eq 0 ]] && _tcpd="\033[1;31m[OFF]" || _tcpd="\033[0;31m[\033[0;32mON\033[0;31m] "
	[[ "$(cat /etc/pam.d/common-password | grep ChuKK-SCRIPT | wc -l)" != '0' ]] && _fv="\033[0;31m[\033[0;32mON\033[0;31m]" || _fv="\033[1;31m[OFF]"
	[[ -e /etc/.hosts.original ]] && _ADS="\033[0;31m[\033[0;32mON\033[0;31m]" || _ADS="\033[1;31m[OFF]"
	[[ "$(echo -e "$_portbaSE"  | grep 'docker' | wc -l)" != '0' ]] && chiselsts="\033[1;32m[ON]" || chiselsts="\033[1;31m[OFF]"

		for instalcolor in `echo "badvpn apacheport shadow shadowr squid dropbear openvpn stunnel shadowsocks sockspy v2ray cfa trojan pshiphon udps tcp xray webmin slowdns sslh socks udp-hysteria udp-custom udp-zipvpn sshl webs socks"`; do
			[[ $(cat ${sfile[log]}|grep $instalcolor) != "" ]] && declare -A proto[$instalcolor]+="\e[1;97m" || declare -A proto[$instalcolor]+="\e[1;97m"
			[[ $(cat ${sfile[log]}|grep $instalcolor) != "" ]] && declare -A port[$instalcolor]+="\e[1;31m[OFF]" || declare -A port[$instalcolor]+="\e[1;32m[ON]"
		done
		function retur(){ read -p "Enter" && protocolos ; }
	
	msg -bar
	print_center -ama "${cor[5]}${sxz[0]} PROTOCOLOS ${sxz[0]} "
	msg -bar
	echo -e "\033[0;35m [\033[0;36m1\033[0;35m]\033[0;33m${flech} ${cor[3]}SQUID         $squid \033[0;35m [\033[0;36m11\033[0;35m]\033[0;33m${flech} ${cor[3]}PSIPHON SERVER $pps"
	echo -e "\033[0;35m [\033[0;36m2\033[0;35m]\033[0;33m${flech} ${cor[3]}DROPBEAR      $dropb \033[0;35m [\033[0;36m12\033[0;35m]\033[0;33m${flech} ${cor[3]}TCP DNS        \033[0;33m(#BETA)"
	echo -e "\033[0;35m [\033[0;36m3\033[0;35m]\033[0;33m${flech} ${cor[3]}OPENVPN       $openvpn \033[0;35m [\033[0;36m13\033[0;35m]\033[0;33m${flech} ${cor[3]}FIX PASSWD"
	echo -e "\033[0;35m [\033[0;36m4\033[0;35m]\033[0;33m${flech} ${cor[3]}SSL/TLS       $stunel \033[0;35m [\033[0;36m14\033[0;35m]\033[0;33m${flech} ${cor[3]}SlowDNS        $P1"
	echo -e "\033[0;35m [\033[0;36m5\033[0;35m]\033[0;33m${flech} ${cor[3]}SHADOWSOCKS-R $shadow \033[0;35m [\033[0;36m15\033[0;35m]\033[0;33m${flech} ${cor[3]}SSL->PYTHON   ${socksA}" #\033[0;33m(#BETA)"
	echo -e "\033[0;35m [\033[0;36m6\033[0;35m]\033[0;33m${flech} ${cor[3]}SHADOWSOCKS   $user_t \033[0;35m [\033[0;36m16\033[0;35m]\033[0;33m${flech} ${cor[3]}SSLH Multiplex $sslh"
	echo -e "\033[0;35m [\033[0;36m7\033[0;35m]\033[0;33m${flech} ${cor[3]}PROXY PYTHON  $socks \033[0;35m [\033[0;36m17\033[0;35m]\033[0;33m${flech} ${cor[3]}OVER WEBSOCKET \033[0;33m(#BETA)"
	echo -e "\033[0;35m [\033[0;36m8\033[0;35m]\033[0;33m${flech} ${cor[3]}V2RAY SWITCH  $v2ray \033[0;35m [\033[0;36m18\033[0;35m]\033[0;33m${flech} ${cor[3]}SOCKS5         \033[0;33m(#BETA)"
	echo -e "\033[0;35m [\033[0;36m9\033[0;35m]\033[0;33m${flech} ${cor[3]}CFA ( CLASH ) $xclash\033[0;35m  [\033[0;36m19\033[0;35m]\033[0;33m${flech} ${cor[3]}Protocolos UDP  $_pidUDP"
	echo -e "\033[0;35m [\033[0;36m10\033[0;35m]\033[0;33m${flech} ${cor[3]}TROJAN-GO    $pTROJ \033[0;35m [\033[0;36m20\033[0;35m]\033[0;33m${flech} ${cor[5]}FUNCIONES EN DISEÑO!"
	msg -bar
	print_center -ama "${cor[5]}${sxz[2]} HERRAMIENTAS GENERALES ${sxz[2]} "
	msg -bar
	echo -e "\033[0;35m [\033[0;36m21\033[0;35m]\033[0;33m ${flech} ${cor[3]}BLOCK TORRENT        \033[0;35m [\033[0;36m22\033[0;35m]\033[0;33m ${flech} ${cor[3]}BadVPN   $_badvpn"
	echo -e "\033[0;35m [\033[0;36m23\033[0;35m]\033[0;33m ${flech} ${cor[3]}TCP (BBR|Plus) $_tcpd \033[0;35m [\033[0;36m24\033[0;35m]\033[0;33m ${flech} ${cor[3]}FAILBAN  $fail_b"
	echo -e "\033[0;35m [\033[0;36m25\033[0;35m]\033[0;33m ${flech} ${cor[3]}ARCHIVO ONLINE \033[0;31m[\033[0;32m${portFTP}\033[0;31m]  \033[0;35m [\033[0;36m26\033[0;35m]\033[0;33m ${flech} ${cor[3]}UP|DOWN SpeedTest " #\033[0;35m [\033[0;36m0\033[0;35m]\033[0;33m ? $(msg -bra "\e[3;33m[ SALIR ]\e[0m")"
	echo -e "\033[0;35m [\033[0;36m27\033[0;35m]\033[0;33m ${flech} ${cor[3]}DETALLES DEL VPS     \033[0;35m [\033[0;36m28\033[0;35m]\033[0;33m ${flech} ${cor[3]}Block ADS $_ADS" #\033[0;35m [\033[0;36m0\033[0;35m]\033[0;33m ? $(msg -bra "\e[3;33m[ SALIR ]\e[0m")"
	echo -e "\033[0;35m [\033[0;36m29\033[0;35m]\033[0;33m ${flech} ${cor[3]}DNS CUSTOM (NETFLIX) \033[0;35m [\033[0;36m30\033[0;35m]\033[0;33m ${flech} ${cor[3]}FIREWALLD CUSTOM" #\033[0;35m [\033[0;36m0\033[0;35m]\033[0;33m ? $(msg -bra "\e[3;33m[ SALIR ]\e[0m")"
#	echo -e "\033[0;35m [\033[0;36m31\033[0;35m]\033[0;33m ${flech} ${cor[3]}Fix PassWD VULTR ${_fv} "
	enter
	x=$(selection_fun 30)
	case $x in
	 0) start_menu ;;
	 1) . ${sfile[p]} --squid && retur;;
	 2) . ${sfile[p]} --dropbear && retur;;
	 3) . ${sfile[p]} --openvpn && retur;;
	 4) . ${sfile[p]} --stunnel && retur;;
	 5) . ${sfile[p]} --shadowsocks && retur;;
	 6) . ${sfile[p]} --shadowsocks-r && retur;;
	 7) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/SockPython.sh) && retur;;
	 8)
		fun_banners --v2ray
		source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) "--v2ray"
	 ;;
	 9) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) -c && retur;;
	 10) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) -t && retur;;
	 11) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) -p && retur;;
	 12) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) --tcp && retur;;
	 13) . ${sfile[p]} --webmin && retur;;
	 14) . ${sfile[p]} --slowdns && retur;;
	 15) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/autoconfig.sh) && retur;;
	 16) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/sslh-back3.sh) && retur;;
	 17) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/socks5.sh) && retur;;
	 19) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/menu_inst/UDPserver.sh) && retur;;
	  18) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) "18" ;;
	 20) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) "20" ;;
	 21|30) source <(curl -sSL https://gitlab.com/patomods/patobase/-/raw/main/sources/blockT.sh) && retur;;
	 22) . ${sfile[p]} --badvpn && retur;;
	 23) . ${sfile[p]} --tcp && retur;;
	 24) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) "24" && retur;;
	 25) herramientas --sftpserver && retur;;
	 26) configuraciones --speed-test && retur;;
	 27) herramientas --detallesvps && retur;;
	 28) herramientas --blockads && retur;;
	 29) source <(curl -sSL https://gitlab.com/dxnpatx/mfiles/-/raw/main/codigos/menu_inst) "18" ;;
	# 30) herramientas --blockbT && retur;;
	#31) herramientas  ;;
	#32) . ${sfile[p]} --squid ;;
	esac
	}

case `selection_fun 8` in
 #1)usercodes;;
 1). ${sfile[usrcd]};;
 8)check_update;;
 2)configuraciones --optimizar-vps;;
 3)monitorhtop;;
 7)creditos;;
 4)protocolos;;
# 4). ${sdir[0]}/menu_inst;;
 5)configuraciones;;
 6)check_update;;
 0)msg -m "baaaai";;
 *)start_menu;;
esac
}



if [ \( -z "$1" \) -a \( ! -n "$1" \) ]; then
	start_menu
else
	clear
fi
