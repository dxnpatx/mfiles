#!/bin/bash
export drowkid='✧ | ᴅʀᴏᴡᴋɪᴅ | ✧'

        for(( x=1;x<254;x++ )); do
                color[$x]="\e[38;5;${x}m"
        done

        for(( x=1;x<8;x++ )); do
                color[a$x]="\e[1;3${x}m"
        done

cor=( "\033[0m" "\033[1;34m" "\033[1;32m" "\033[1;37m" "\033[1;36m" "\033[1;33m" "\033[1;35m" )

function ip(){
	[[ `wget -qO- ifconfig.me` != `wget -qO- ipv4.icanhazip.com` ]] && {
			ip=''
	} || {
			ip=$(echo `wget -qO- ipv4.icanhazip.com`|grep `wget -qO- ifconfig.me`)
	}
}

function msg(){
local COLOR[0]='\033[1;37m' COLOR[1]='\e[93m' COLOR[2]='\e[32m' COLOR[3]='\e[31m' COLOR[4]='\e[34m' COLOR[5]='\e[95m' COLOR[6]='\033[1;97m' COLOR[7]='\033[36m' NEGRITO='\e[1m' SEMCOR='\e[0m'
	case $1 in
	 -sep) echo -e "${color[124]}==============================================" ;;
          -st) echo -e "\033[38;5;239m\e[100m\e[97m  ${2}  \e[0m";;
	 -bar) echo -e "${color[186]}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${color[n]}";;
	  -ne) echo -ne " \e[1;30m[\e[1;33m•\e[1;30m] \e[1;33m$2${color[2]} ";read $3 ;;
	   -r) echo -e "${color[1]}$2${color[n]}" ;;
	   -c) echo -e "${color[6]}$2${color[n]}";;
	   -a) echo -e "${color[3]}$2${color[n]}";;
	 -ama)cor="${COLOR[3]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	-verm)cor="${COLOR[3]}${NEGRITO}[!] ${COLOR[1]}" && echo -e "${cor}${2}${SEMCOR}";;
       -verm2)cor="${COLOR[3]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	 -azu)cor="${COLOR[6]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	-azuc)cor="${COLOR[7]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	-verd)cor="${COLOR[2]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	  -az)cor="${COLOR[4]}${NEGRITO}" && echo -e "${cor}${2}${SEMCOR}";;
	 -bra) echo -e "\e[1m\e[1;37m$2\e[0m";;
	esac
}

function enter(){
	msg -bar
	printf "%13s \033[0;35m [\033[0;36m0\033[0;35m]\033[0;33m ${flech} $(msg -bra "\033[1;41m[ regresar ]\e[0m")\n"
	msg -bar
}

function fun_bar(){
comando[0]="$1"
comando[1]="$2"
 (
[[ -e $HOME/fim ]] && rm $HOME/fim
${comando[0]} -y > /dev/null 2>&1
${comando[1]} -y > /dev/null 2>&1
touch $HOME/fim
 ) > /dev/null 2>&1 &
echo -ne "\033[1;33m ["
while true; do
   for((i=0; i<18; i++)); do
   echo -ne "\033[1;31m##"
   sleep 0.1s
   done
   [[ -e $HOME/fim ]] && rm $HOME/fim && break
   echo -e "\033[1;33m]"
   sleep 1s
   tput cuu1
   tput dl1
   echo -ne "\033[1;33m ["
done
echo -e "\033[1;33m]\033[1;31m -\033[1;32m 100%\033[1;37m"
}

function selection_fun(){
local selection="null" range
	for((i=0; i<=$1; i++)); do range[$i]="$i "; done
	while [[ ! $(echo -e "${range[*]}"|grep -w "$selection") ]]; do
	echo -ne "\033[1;30m ╰► Seleccione su opción: " >&2
	read selection
	tput cuu1 >&2 && tput dl1 >&2
	done
echo $selection
}

function menu_func(){
	function cx(){ echo -ne "\033[0;35m [\033[0;36m$num\033[0;35m]\033[0;33m " ; }
  local options=${#@} array
  for((num=1; num<=$options; num++)); do
    array=(${!num})
    case ${array[0]} in
       "-vm") cx && echo -e "\033[1;33m[!]\033[1;31m ${array[@]:1}";;
       "-st") echo -e "${color[186]}━━━━━━━━━━━━━━━\e[100m\e[97m  ${array[@]:1}  \e[0m${color[186]}━━━━━━━━━━━━━━━━━\e[0m";;
      "-tit") echo -e "\033[38;5;239m════════\e[100m\e[97m  ${array[@]:1}  \e[0m\e[38;5;239m════════";;
      "-bar") #|-bar2|-bar3|-bar4|-bar5|-bar6|-bar7|-bar8|-bar9|-bar10|-bar11|-bar12|-bar13|-bar14|-bar15|-bar16|-bar17|-bar18|-bar19|-bar20|-bar21|-bar22|-bar23|-bar24|-bar25)
      cx && echo -e " \033[1;37m${array[@]:1}\n$(msg ${array[0]})" ;;
      *) cx && echo -e "${cor[3]} ${array[@]}";;
    esac
  done
}

function print_center(){
if [[ -z $2 ]]; then
    text="$1"
  else
    col="$1"
    text="$2"
  fi

  while read line; do
    unset space
    x=$(( ( 54 - ${#line}) / 2))
    for (( i = 0; i < $x; i++ )); do
      space+=' '
    done
    space+="$line"
    if [[ -z $2 ]]; then
      echo -e "${color[3]}$space"
    else
      echo -e "${color[$col]}$space"
    fi
  done <<< $(echo -e "$text")
}

function continue(){
	msg -bar
	read -p $'\e[38;5;102m==========>> presione enter para continuar <<==========' uxp
}
